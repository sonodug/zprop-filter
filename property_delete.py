import json
import requests
import argparse
from datetime import datetime, timedelta
import base64
import uuid

import urllib3

urllib3.disable_warnings()

# How to use
# !python property_delete.py -i ./to_delete.json
# !python property_delete.py -primId *primitive_id*

# json example:
# {
#     "primitiveIds": [
#         "1",
#         "2",
#         "3"
#     ]
# }

def check_auth_OFFLINE(auth_url, auth_payload):
    auth_at = datetime.now()
    # взять из F12
    access_token = ""
    return (auth_at, access_token)

# Try authorize, get token
def check_auth(auth_url, auth_payload):
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    try:
        response = requests.post(auth_url, data=auth_payload, headers=headers, verify=False)
        print("auth", str(response.status_code))

        auth_at = None
        access_token = None
        if "access_token" in response.json():
            auth_at = datetime.now()
            access_token = response.json()["access_token"]
        else:
            print('Authorization failed ' +
                  response.text + ' fallback to NoAuth')
        return (auth_at, access_token)
    except Exception as err:
        print("Authorization failed: {0}".format(err))

class PropertyFilter:
    def __init__(self):
        self.auth_at = datetime(1900, 1, 1, 0, 0)
        self.access_token = None

        # To config
        self.client_id = "zui-app-shell"
        self.client_secret = ""
        self.code = ''

        self.zifomobject_url = "https://idpbn-bgpz.apps.bgpz-yc-okd.private.ziiot.ru/zif-om-object"

    def refresh_token(self):
        auth_at = 'https://idpbn.apps.bgpz-yc-okd.private.ziiot.ru/auth/realms/idpbn_bgpz/protocol/openid-connect/token'
        auth_payload = ('grant_type=authorization_code&client_id=' + self.client_id +
                        '&redirect_uri=https%3A%2F%2Fidpbn-bgpz.apps.bgpz-yc-okd.private.ziiot.ru%2F' +
                        '&code=' + self.code)
        self.auth_at, self.access_token = check_auth_OFFLINE(auth_at, auth_payload)

    def delete_property_by_id(self, property_id):
        if (datetime.now() - self.auth_at) > timedelta(seconds=30):
            self.refresh_token()

        delete_url = self.zifomobject_url + f'/properties/{property_id}'
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + self.access_token
        }
        response = requests.delete(delete_url, headers=headers, verify=False)

        if response.status_code == 204:
            print(f'Property ID = {property_id} successfully deleted')
        else:
            print(f'Error with deleteting property with ID = {property_id}. Code: {response.status_code}')
        print()

    def filter_properties(self, primitive_id):
        # try:
        if (datetime.now() - self.auth_at) > timedelta(seconds=30):
            self.refresh_token()

        url = self.zifomobject_url + '/properties'
        params = {
            'ispaged': False, # Получаем все элементы?
            'filterMode': 'ONLYELEMENT',
            'hierarchyFilterType': 'Or',
            'onlyRoot': True,
            'primitiveId': f"{primitive_id}",
            'includeDeleted': False,
            'calculateTotalCount': True
        }
        headers = {
            'Content-Type': 'application/json',
            'accept': 'application/json',
            'Authorization': 'Bearer ' + self.access_token
        }
        response = requests.get(url, params=params, headers=headers, verify=False)
        # print( response )
        # print( response.json() )
        # return
        properties = response.json()["content"]

        #  Get property ID and delete it
        for property_data in properties:
            property_id = property_data['id']
            self.delete_property_by_id(property_id)
        # except Exception as e:
        #     print(e)

def get_args():
    parser = argparse.ArgumentParser(
        description="Property filter",
        epilog="Examples: python property_delete.py -primId *your_primitive_id*    |   "
               "python property_delete.py -i ./to_delete.json")

    parser.add_argument("-primId", "--primitiveId", help="",
                            type=str, default="default")

    parser.add_argument("-i", "--input", help="path to json file with primitiveIds data",
                            metavar="path", type=str, default="default")
    return parser.parse_args()

def run(args, filter):
    # try:
    if (args.input != "default" and args.primitiveId != "default"):
        print("Incorrect format, use !python property_delete.py -h for help")
        return

    if (args.input == "default"):
        if (args.primitiveId == "default"):
            print("Incorrect primitive id input, use !python property_delete.py -h for help")
            return
        else:
            filter.filter_properties(args.primitiveId)
    else:
        path = args.input
        with open(path, 'r', encoding='utf-8') as f:
            data = json.load(f)
            prim_ids = data["primitiveIds"]
            for id in prim_ids:
                filter.filter_properties(id)

    # except Exception as e:
    #     print(e)

def main():
    global args
    args = get_args()
    filter = PropertyFilter()
    run(args, filter)

if __name__ == '__main__':
    main()